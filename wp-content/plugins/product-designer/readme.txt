=== Product Designer ===
	Contributors: PickPlugins
	Donate link: https://www.pickplugins.com/item/product-designer/?ref=wordpress.org
	Tags: Product Designer, T-Shirt Designer, T-Shirt, Designer, T-Shirt Designer woocommerce, woocommerce, product customizer
	Requires at least: 3.8
	Tested up to: 4.9
	Stable tag: 1.0.8
	License: GPLv2 or later
	License URI: http://www.gnu.org/licenses/gpl-2.0.html

	Awesome Product Designer for WooCommerce.

== Description ==
This plugin allows you to display an awesome Product Designer on your website via short-code anywhere in page,
add unlimited clip art via custom post and display on designer and easy to use on Product.
Also text can be use with fancy font family, font size and color.


### Product Designer by http://pickplugins.com

* [See Premium &raquo;](https://www.pickplugins.com/item/product-designer/?ref=wordpress.org)
* [Live Demo &raquo;](https://www.pickplugins.com/demo/product-designer/?ref=wordpress.org)
* [Documentation &raquo;](https://www.pickplugins.com/documentation/product-designer/faq/how-to-display-product-designer-editor/?ref=wordpress.org)
* [support &raquo;](https://www.pickplugins.com/support/?ref=wordpress.org)


# Plugin Features

* WooCommerce ready.
* Unlimited cliparts with category & ajax pagination.
* Text art.
* Unlimited product sides.
* Design preview.
* 50+ hand pick stylish google font.

# Premium Features

* Unlimited Pre Saved templates.
* Upload custom cliparts.
* Curve Text.
* Unlimited Quotes Text.
* QR code.
* Barcode.

# Video tutorials

* [How to Install?](https://www.youtube.com/watch?v=DNG07bincDk)
* [How to Configure?](https://www.youtube.com/watch?v=T_ppBuGcxnQ)
* [Tour Guide](https://www.youtube.com/watch?v=qK2MyS10uFI)
* [How to add Cliparts, QR code, Barcode on design?](https://www.youtube.com/watch?v=U7_UDxjo6bk)
* [How to add Text & Curve Text on Design?](https://www.youtube.com/watch?v=c_bOmHD8--w)



== Installation ==

1. Install as regular WordPress plugin.<br />
2. Go your plugin setting via WordPress Dashboard and find "<strong>Product Designer</strong>" activate it.<br />

After activate plugin you will see "Product Designer" menu at left side on WordPress dashboard.<br />

<br />
<strong>How to use on page.</strong><br />


Use following short-code to display designer anywhere in page content. `[product_designer]`<br />

Before starting you need to add some clip art via custom post "Clip Art" with thumbnail image.






== Screenshots ==

1. screenshot-1
2. screenshot-2
3. screenshot-3
4. screenshot-4
5. screenshot-5
6. screenshot-6



== Changelog ==

	= 1.0.8 =
    * 02/08/2018 - fix - More string added to translate.

	= 1.0.7 =
    * 23/07/2018 - fix - Translation issue fixed.

	= 1.0.6 =
    * 03/07/2018 - fix - Canvas Item sides meta field fixed.
    * 03/07/2018 - update - Update video tutorials.

	= 1.0.5 =
    * 06/06/2018 - update - Re-write plugin.

	= 1.0.4 =
    * 30/03/2017 - fix - designer broken issue fixed.

	= 1.0.3 =
    * 02/03/2016 - add - translation added.

	= 1.0.2 =
    * 12/12/2016 - add - added help page.

	= 1.0.1 =
    * 29/11/2016 - add - added shapes.

	= 1.0.0 =
    * 28/11/2016 Initial release.
