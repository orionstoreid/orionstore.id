<?php

/*
* @Author 		pickplugins
* Copyright: 	2015 pickplugins
*/

if ( ! defined('ABSPATH')) exit;  // if direct access


    ?>
    <div class="preview">
        <div class="preview-img">
            <span class="preview-close"><i class="fa fa-times" aria-hidden="true"></i></span>
            <div class="img"></div>
        </div>
    </div>