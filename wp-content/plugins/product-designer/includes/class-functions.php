<?php

/*
* @Author 		ParaTheme
* Copyright: 	2015 ParaTheme
*/

if ( ! defined('ABSPATH')) exit;  // if direct access 	


class class_product_designer_functions  {
	
	
    public function __construct(){

		
		//add_action('add_meta_boxes', array( $this, 'clipart_meta_boxes' ));
	
		
    }
	
	public function tutorials(){

		$tutorials[] = array(
							'title'=>__('How to Install?', product_designer_textdomain),
							'video_id'=>'DNG07bincDk',
							'source'=>'youtube',
							);
							
		$tutorials[] = array(
							'title'=>__('How to Configure?', product_designer_textdomain),
							'video_id'=>'T_ppBuGcxnQ',
							'source'=>'youtube',
							);							
							
		$tutorials[] = array(
							'title'=>__('Tour Guide', product_designer_textdomain),
							'video_id'=>'qK2MyS10uFI',
							'source'=>'youtube',
							);

		$tutorials[] = array(
			'title'=>__('How to add Cliparts, QR code, Barcode on design?', product_designer_textdomain),
			'video_id'=>'U7_UDxjo6bk',
			'source'=>'youtube',
		);

		$tutorials[] = array(
			'title'=>__('How to add Text & Curve Text on Design?', product_designer_textdomain),
			'video_id'=>'c_bOmHD8--w',
			'source'=>'youtube',
		);



		$tutorials = apply_filters('product_designer_filters_tutorials', $tutorials);		

		return $tutorials;

		}	
	
	
	public function faq(){



		$faq['core'] = array(
							'title'=>__('Core', product_designer_textdomain),
							'items'=>array(



											array(
												'question'=>__('How to display Product Designer Editor?', product_designer_textdomain),
												'answer_url'=>'https://www.pickplugins.com/documentation/product-designer/faq/how-to-display-product-designer-editor/',

												),




											),

								
							);

					
		
		
		$faq = apply_filters('product_designer_filters_faq', $faq);		

		return $faq;

		}		
	
	

}


new class_product_designer_functions();

